<?php

namespace App\Providers;

use App\Listeners\ChangeFileStatus;

class RabbitEventsServiceProvider extends \Nuwber\Events\RabbitEventsServiceProvider
{

    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'file.compressed' => [
            ChangeFileStatus::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot(): void
    {
        parent::boot();
        //
    }
}
