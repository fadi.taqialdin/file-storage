<?php

namespace App\Listeners;

use App\Models\File;
use Illuminate\Support\Facades\Storage;

class ChangeFileStatus
{
    public function handle(array $payload)
    {
        if(!isset($payload['id']) || !isset($payload['compressed'])){
            return false;
        }

        $file = File::query()->find($payload['id']);
        if(!$file){
            return false;
        }

        $zipName = $file->name.'.zip';
        Storage::disk('local')->put($zipName, base64_decode($payload['compressed']));

        $file->path = $zipName;
        $file->compressed = true;
        $file->save();

        // Stopping The Propagation Of The Event
        return false;
    }
}
