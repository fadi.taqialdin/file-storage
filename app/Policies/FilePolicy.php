<?php

namespace App\Policies;

use App\Models\File;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class FilePolicy
{
    use HandlesAuthorization;

    public function viewAny(User $current): bool {
        return true;
    }

    public function create(User $current): bool {
        return true;
    }

    public function view(User $current, File $target): bool {
        return $target->user_id == $current->id;
    }

    public function update(User $current, File $target): bool {
        return $target->user_id == $current->id;
    }

    public function delete(User $current, File $target): bool {
        return $target->user_id == $current->id;
    }
}
