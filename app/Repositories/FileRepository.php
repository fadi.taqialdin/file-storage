<?php
namespace App\Repositories;

use App\Http\Requests\FileRequest;
use App\Models\File;
use Illuminate\Support\Facades\Storage;

class FileRepository {

    public static function store(FileRequest $request){
        $file = new File();
        $file->user_id = auth()->user()->id;
        $file->path = $request->file('file')->store('');
        $file->name = pathinfo($request->file('file')->getClientOriginalName(), PATHINFO_FILENAME);
        $file->format = $request->file('file')->getClientOriginalExtension();
        $file->size = number_format($request->file('file')->getSize() / (1024 * 1024),2);
        $file->save();

        //FileUploaded::dispatch(Storage::disk('local')->get($file->path));
        publish('file.uploaded', [
            'id' => $file->id,
            'name' => $file->name.'.'.$file->format,
            'raw' => base64_encode(Storage::disk('local')->get($file->path))
        ]);
    }
    public static function update(File $file, FileRequest $request){
        $file->fill($request->validated());
        $file->save();
    }
    public static function destroy(File $file){
        Storage::delete($file->path);
        $file->delete();
    }

}
