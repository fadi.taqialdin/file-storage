<?php

namespace App\Http\Controllers;

use App\DataTables\FileDataTable;
use App\Http\Requests\FileRequest;
use App\Models\File;
use App\Repositories\FileRepository;
use Illuminate\Support\Facades\Storage;
use Laracasts\Flash\Flash;

class FilesController extends Controller
{
    public function __construct() {
        $this->authorizeResource(File::class);
    }

    public function index(FileDataTable $dataTable) {
        return $dataTable->render('files.index');
    }

    public function store(FileRequest $request) {
        FileRepository::store($request);
        Flash::success(__('Saved!'));
        return redirect(route('files.index'));
    }

    public function show(File $file) {
        return Storage::download($file->path, $file->name.'.zip');
    }

    public function update(FileRequest $request, File $file) {
        FileRepository::update($file, $request);
        Flash::success(__('Saved!'));
        return redirect(route('files.index'));
    }

    public function destroy(File $file) {
        FileRepository::destroy($file);
        Flash::success(__('Deleted!'));
        return redirect(route('files.index'));
    }
}
