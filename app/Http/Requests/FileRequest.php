<?php

namespace App\Http\Requests;

use App\Models\File;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Request;

class FileRequest extends FormRequest {
    public function rules(): array {
        switch($this->method()){
            case Request::METHOD_POST:
                return File::createRules();
            case Request::METHOD_PUT:
                return File::editRules();
        }
        return [];
    }
}
