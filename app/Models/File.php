<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property int $id
 * @property string $path
 * @property int $user_id
 * @property string $name
 * @property string $format
 * @property double $size
 */
class File extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'user_id',
        'path',
        'name',
        'format',
        'size',
        'compressed',
    ];

	public static function createRules(): array {
        return [
            'file' => 'required|file|max:8192'
        ];
	}
	public static function editRules(): array {
        return [
            'name' => 'required|string',
        ];
	}

	public function user(): BelongsTo {
        return $this->belongsTo(User::class);
    }
}
