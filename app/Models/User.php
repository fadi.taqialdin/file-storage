<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

/**
 * @property int $id
 * @property string $username
 */
class User extends Authenticatable
{
    use HasApiTokens, Notifiable, HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'username',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    public function setPasswordAttribute($value) {
        if($value) {
            $this->attributes['password'] = bcrypt($value);
        }
    }

    public function files(): HasMany {
        return $this->hasMany(File::class);
    }

    public static function registerRules(): array {
        return [
            'username' => 'required|string|unique:users,username',
            'password' => 'required|string|confirmed',
        ];
    }

    public static function loginRules(): array {
        return [
            'username' => 'required|string',
            'password' => 'required|string',
        ];
    }
}
