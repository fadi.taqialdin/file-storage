<?php
namespace App\DataTables;

use App\Models\File;
use Illuminate\Database\Eloquent\Builder;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;

class FileDataTable extends BaseDataTable {

    public function dataTable($query): EloquentDataTable {
        $dataTable = new EloquentDataTable($query);
        return $dataTable
            ->addIndexColumn()
            ->addColumn('action', 'files.datatables_actions')
            ->editColumn('created_at', function($data){
                return $data->created_at->format('Y-m-d H:i:s');
            })
            ->editColumn('compressed', function($data){
                return $data->compressed?'Ready ZIP':'Compressing...';
            });
    }

    public function query(File $model): Builder {
        return $model->newQuery()->where('user_id', auth()->user()->id);
    }

    protected function getColumns() : array {
        return [
            new Column(['title' => __('Created at'), 'data' => 'created_at']),
            new Column(['title' => __('Name'), 'data' => 'name']),
            new Column(['title' => __('Format'), 'data' => 'format']),
            new Column(['title' => __('Size (MB)'), 'data' => 'size']),
            new Column(['title' => __('Status'), 'data' => 'compressed']),
        ];
    }

    protected function filename(): string {
        return 'files_'.time();
    }
}
