<?php
namespace App\DataTables;

use Yajra\DataTables\Html\Builder;
use Yajra\DataTables\Services\DataTable;

abstract class BaseDataTable extends DataTable {

    protected abstract function getColumns() : array;

    public function html(): Builder {
        return $this->builder()
                    ->setTableId(last(explode('\\', get_class($this))))
                    ->columns($this->getColumns())
                    ->addAction(['width' => '120px', 'printable' => false, 'title' => __('Actions')])
                    ->minifiedAjax()
                    ->parameters(
                        [
                            'dom'       => 'Bfrtip',
                            'stateSave' => true,
                            'order'     => [[0, 'desc']],
                            'buttons'   => [
                                [
                                    'extend'    => 'excel',
                                    'className' => 'btn btn-default btn-sm no-corner',
                                    'text'      => '<i class="fa fa-download"></i> ' . __('Export') . '',
                                ],
                                [
                                    'extend'    => 'print',
                                    'className' => 'btn btn-default btn-sm no-corner',
                                    'text'      => '<i class="fa fa-print"></i> ' . __('Print') . '',
                                ],
                                [
                                    'extend'    => 'reload',
                                    'className' => 'btn btn-default btn-sm no-corner',
                                    'text'      => '<i class="fa fa-refresh"></i> ' . __('Reload') . '',
                                ]
                            ],
                            'language'  => [
                                'url' => asset('assets/vendor/datatables/i18n/english.json'),
                            ],
                        ]
                    );
    }
}
