"use strict";

$(document).ready(function () {
    "use strict";

    window.gullUtils = {
        isMobile: function isMobile() {
            return window && window.matchMedia("(max-width: 767px)").matches;
        },
        changeCssLink: function changeCssLink(storageKey, fileUrl) {
            localStorage.setItem(storageKey, fileUrl);
            location.reload();
        }
    };

    $(".perfect-scrollbar, [data-perfect-scrollbar]").each(function (index) {
        var $el = $(this);
        var ps = new PerfectScrollbar(this, {
            suppressScrollX: $el.data("suppress-scroll-x"),
            suppressScrollY: $el.data("suppress-scroll-y")
        });
    });

});

$(window).on('load', function() {
    'use strict';
    $('.loadscreen').delay(500).fadeOut();
});

function withPrecondition(callback) {
    let isDone = false;
    return function (event) {
        if (isDone === true) {
            isDone = false;
            return;
        }
        event.preventDefault();
        let element = this;
        callback(function () {
            isDone = true;
            $(element).trigger(event.type);
        });
    }
}

$('.main-content-wrap').on('click', '.delete', withPrecondition(function (finishCallback) {
    swal({
        title: 'Are you sure?!',
        text: 'the file will be deleted permanently/',
        type: 'warning',
        showCancelButton: true,
        cancelButtonText: 'Cancel',
        confirmButtonText: 'Delete',
        confirmButtonClass: 'btn btn-danger mr-5',
        cancelButtonClass: 'btn btn-secondary',
        buttonsStyling: false
    }).then(function () {
        finishCallback();
    }, function () {
    });
}));

$('.main-content-wrap').on('submit', 'form', function (e) {
    $('.ladda-button').each(function () {
        Ladda.create(this).start();
    });
});

document.getElementById("file").onchange = function() {
    if(this.files[0].size > 1024*1024*8){
        alert("Maximum file size allowed is 8MB!");
        this.value = "";
    }
};
