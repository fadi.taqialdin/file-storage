## About

This application made to provide basic files management.

## How to run

1. Install the application dependencies using docker
```
docker-compose up
```
2. Please wait about a minute for composer installation and db migrations get ready. 

3. Visit `http://127.0.0.1:8000`.

## Testing

You can run the tests using `php artisan test` command or `composer test` command.

## Notice

This app depends on [FileCompression](https://gitlab.com/fadi.taqialdin/file-compression) service, uploaded files will be disabled until they got handled by FileCompression service.
