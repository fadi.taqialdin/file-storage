<div class="app-admin-wrap layout-sidebar-large">
    <div class="main-header">
        <div class="logo-corner ml-48">
            <h5>Welcome <span class="username">{{ ucfirst(auth()->user()->username) }}</span>!</h5>
        </div>
        <div style="margin: auto"></div>
        <div class="header-part-right">
            <a class="mr-48" href="{{ route('logout') }}">
                <i class="i-Power-3 mr-1"></i> Logout
            </a>
        </div>
    </div>
</div>
