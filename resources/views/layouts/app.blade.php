<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <link href="{{ asset('assets/vendor/bootstrap/bootstrap.min.css') }}" rel="stylesheet" />
        <link href="{{ asset('assets/vendor/perfect-scrollbar/perfect-scrollbar.min.css') }}" rel="stylesheet" />
        <link href="{{ asset('assets/vendor/sweetalert2/sweetalert2.min.css') }}" rel="stylesheet" />
        <link href="{{ asset('assets/vendor/ladda/ladda-themeless.min.css') }}" rel="stylesheet" />
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
        <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css" rel="stylesheet" />
        <link href="https://cdn.datatables.net/buttons/1.3.1/css/buttons.bootstrap.min.css" rel="stylesheet" />

        <link href="{{ asset('assets/css/theme.css') }}" rel="stylesheet" />
        <link href="{{ asset('assets/css/app.css') }}" rel="stylesheet" />

        @stack('styles')
    </head>
    <body class="text-left">

        <div class="app-admin-wrap layout-sidebar-large">
            @include('layouts.header')
            <div class="main-content-wrap d-flex flex-column">
                @include('flash::message')
                @yield('content')
            </div>
        </div>
        <div class="loadscreen">
            <div class="loader">
                <div class="loader-bubble loader-bubble-info d-block"></div>
            </div>
        </div>

        <script src="{{ asset('assets/vendor/jquery/jquery-3.3.1.min.js') }}"></script>
        <script src="{{ asset('assets/vendor/bootstrap/bootstrap.min.js') }}"></script>
        <script src="{{ asset('assets/vendor/perfect-scrollbar/perfect-scrollbar.min.js') }}"></script>
        <script src="{{ asset('assets/vendor/sweetalert2/sweetalert2.min.js') }}"></script>
        <script src="{{ asset('assets/vendor/ladda/spin.min.js') }}"></script>
        <script src="{{ asset('assets/vendor/ladda/ladda.min.js') }}"></script>

        <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.bootstrap.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.colVis.min.js"></script>
        <script src="{{ asset('assets/vendor/datatables/buttons.server-side.js') }}"></script>

        <script src="{{ asset('assets/js/app.js') }}"></script>

        @stack('scripts')
    </body>
</html>
