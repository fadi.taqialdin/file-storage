@extends('layouts.app')

@section('content')
    <div class="content">
        <div class="card mb-4">
            <div class="card-body">
                <div class="card-title">
                    <h3 class="float-left">Files</h3>
                    @include('files.upload')
                </div>
                @include('layouts.datatable')
            </div>
        </div>
    </div>
@endsection
