<div class="float-right">
    <button data-toggle="modal" data-target="#upload-file-modal" class="btn btn-success">
        <i class="fa fa-plus"></i> Upload
    </button>
</div>
<div id="upload-file-modal" data-backdrop="static" data-keyboard="false" class="modal" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <h3 class="text-center mt-44">Select file</h3>
            <form action="{{ route('files.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <input class="form-control" name="file" id="file" type="file" required>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-block ladda-button" type="button" data-dismiss="modal">Close</button>
                    <button class="btn btn-success ladda-button" data-style="expand-left" type="submit">Upload</button>
                </div>
            </form>
        </div>
    </div>
</div>
