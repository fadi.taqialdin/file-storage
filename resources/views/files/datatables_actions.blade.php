@php($file = \App\Models\File::query()->find($id))

@if($file->compressed)
    @include('files.edit')

    <a href="{{route('files.show', $file)}}" class="btn btn-sm btn-secondary" download>
        <i class="fa fa-download"></i>
    </a>

    {!! Form::open(['route' => ['files.destroy', $file], 'method' => 'delete', 'style' => 'display: inline;']) !!}
    {!! Form::button('<i class="fa fa-trash"></i>', [
        'type' => 'submit',
        'class' => 'btn btn-danger btn-sm m-1 delete'
    ]) !!}
    {!! Form::close() !!}
@endif

