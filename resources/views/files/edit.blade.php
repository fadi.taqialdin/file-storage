<button data-toggle="modal" data-target="#edit-file-modal-{{$file->id}}" class="btn btn-info btn-sm m-1">
    <i class="fa fa-edit"></i>
</button>
<div id="edit-file-modal-{{$file->id}}" data-backdrop="static" data-keyboard="false" class="modal" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <h3 class="text-center mt-44">Change File Name</h3>
            <form action="{{ route('files.update', $file) }}" method="POST">
                @csrf
                @method('PUT')
                <div class="modal-body">
                    <input class="form-control" type="text" id="name" name="name" placeholder="Name" value="{{$file->name}}" style="width: 100%;" required>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-block ladda-button" data-style="expand-left" type="button" data-dismiss="modal">Close</button>
                    <button class="btn btn-success ladda-button" data-style="expand-left" type="submit">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>
