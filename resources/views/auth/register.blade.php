@extends('layouts.guest')

@section('content')

    <div class="col-md-12">
        <div class="p-4">
            <h1 class="mb-3 text-18">Register</h1>

            <!-- Validation Errors -->
            @if ($errors->any())
                <div class="mb-4">
                    <div class="font-medium text-red-600">
                        {{ __('Whoops! Something went wrong.') }}
                    </div>

                    <ul class="mt-3 list-disc list-inside text-sm text-red-600">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <form method="POST" action="{{ route('register') }}">
                @csrf

                <div class="form-group">
                    <label for="username">Username</label>
                    <input class="form-control form-control-rounded" id="username" type="text" name="username" value="{{old('username')}}" required autofocus>
                </div>

                <div class="form-group">
                    <label for="password">Password</label>
                    <input class="form-control form-control-rounded" id="password" type="password" name="password" required>
                </div>

                <div class="form-group">
                    <label for="password_confirmation">Confirm Password</label>
                    <input class="form-control form-control-rounded" id="password_confirmation" type="password" name="password_confirmation" required>
                </div>

                <button class="btn btn-rounded btn-primary btn-block mt-2"><i class="i-Mail-with-At-Sign"></i> Register</button>
            </form>
            <a class="mt-12 btn btn-rounded btn-outline-primary btn-outline-email btn-block btn-icon-text" href="{{ route('login') }}">
                <i class="i-Lock"></i> Login?
            </a>
        </div>
    </div>

@endsection
