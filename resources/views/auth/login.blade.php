@extends('layouts.guest')

@section('content')

    <div class="col-md-12">
        <div class="p-4">
            <h1 class="mb-3 text-18">Login</h1>

            <!-- Session Status -->
            @if (session('status'))
                <div class="mb-4 font-medium text-sm text-green-600">
                    {{ session('status') }}
                </div>
            @endif

            <!-- Validation Errors -->
            @if ($errors->any())
                <div class="mb-4">
                    <div class="font-medium text-red-600">
                        {{ __('Whoops! Something went wrong.') }}
                    </div>

                    <ul class="mt-3 list-disc list-inside text-sm text-red-600">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <form method="POST" action="{{ route('login') }}">
                @csrf
                <div class="form-group">
                    <label for="username">Username</label>
                    <input class="form-control form-control-rounded" id="username" type="text" name="username" value="{{old('username')}}" required>
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input class="form-control form-control-rounded" id="password" type="password" name="password" required>
                </div>
                <button class="btn btn-rounded btn-primary btn-block mt-2"><i class="i-Lock"></i> Login</button>
            </form>
            <a class="mt-12 btn btn-rounded btn-outline-primary btn-outline-email btn-block btn-icon-text" href="{{ route('register') }}">
                <i class="i-Mail-with-At-Sign"></i> Register?
            </a>
        </div>
    </div>
    {{--<div class="col-md-6 text-center" style="background-size: cover;background-image: url({{ asset('assets/images/bg2.png') }})">
        <div class="pr-3 auth-right">

        </div>
    </div>--}}

@endsection
