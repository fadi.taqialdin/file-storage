<?php

use App\Http\Controllers\Auth\AuthenticatedSessionController;
use App\Http\Controllers\Auth\RegisteredUserController;
use App\Http\Controllers\FilesController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/register', [RegisteredUserController::class, 'create'])
     ->middleware('guest')
     ->name('register');

Route::post('/register', [RegisteredUserController::class, 'store'])
     ->middleware('guest');

Route::get('/login', [AuthenticatedSessionController::class, 'create'])
     ->middleware('guest')
     ->name('login');

Route::post('/login', [AuthenticatedSessionController::class, 'store'])
     ->middleware('guest');

Route::get('/logout', [AuthenticatedSessionController::class, 'destroy'])
     ->middleware('auth')->name('logout');

Route::group(['middleware' => ['auth']], function(){
    Route::get('/', function(){
        return redirect(route('files.index'));
    });
    Route::apiResource('files', FilesController::class);
});

