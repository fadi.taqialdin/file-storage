<?php

namespace Tests\Feature\Auth;

use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

class RegisterTest extends TestCase
{
    use RefreshDatabase;

    public function testUserCanViewARegistrationForm()
    {
        $response = $this->get(route('register'));

        $response->assertSuccessful();
        $response->assertViewIs('auth.register');
    }

    public function testUserCannotViewARegistrationFormWhenAuthenticated()
    {
        $user = User::factory()->make();

        $response = $this->actingAs($user)->get(route('register'));

        $response->assertRedirect(route('files.index'));
    }

    public function testUserCanRegister()
    {
        Event::fake();

        $response = $this->post(route('register'), [
            'username' => 'fadi',
            'password' => '123456',
            'password_confirmation' => '123456',
        ]);

        $this->assertCount(1, $users = User::all());
        $this->assertAuthenticatedAs($user = $users->first());
        $this->assertEquals('fadi', $user->username);
        $this->assertTrue(Hash::check('123456', $user->password));
        Event::assertDispatched(Registered::class, function ($e) use ($user) {
            return $e->user->id === $user->id;
        });
    }

    public function testUserCannotRegisterWithoutUserName()
    {
        $response = $this->post(route('register'), [
            'username' => '',
            'password' => '123456',
            'password_confirmation' => '123456',
        ]);

        $users = User::all();

        $this->assertCount(0, $users);
        $response->assertSessionHasErrors('username');
        $this->assertGuest();
    }

    public function testUserCannotRegisterWithoutPassword()
    {
        $response = $this->post(route('register'), [
            'username' => 'fadi',
            'password' => '',
            'password_confirmation' => '',
        ]);

        $users = User::all();

        $this->assertCount(0, $users);
        $response->assertSessionHasErrors('password');
        $this->assertGuest();
    }

    public function testUserCannotRegisterWithoutPasswordConfirmation()
    {
        $response = $this->post(route('register'), [
            'username' => 'fadi',
            'password' => '123456',
            'password_confirmation' => '',
        ]);

        $users = User::all();

        $this->assertCount(0, $users);
        $response->assertSessionHasErrors('password');
        $this->assertGuest();
    }

    public function testUserCannotRegisterWithPasswordsNotMatching()
    {
        $response = $this->post(route('register'), [
            'username' => 'fadi',
            'password' => '123456',
            'password_confirmation' => 'password',
        ]);

        $users = User::all();

        $this->assertCount(0, $users);
        $response->assertSessionHasErrors('password');
        $this->assertGuest();
    }
}
