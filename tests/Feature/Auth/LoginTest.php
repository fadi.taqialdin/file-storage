<?php

namespace Tests\Feature\Auth;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class LoginTest extends TestCase
{
    use RefreshDatabase;

    public function testUserCanViewALoginForm()
    {
        $response = $this->get(route('login'));

        $response->assertSuccessful();
        $response->assertViewIs('auth.login');
    }

    public function testUserCannotViewALoginFormWhenAuthenticated()
    {
        $user = User::factory()->make();

        $response = $this->actingAs($user)->get(route('login'));

        $response->assertRedirect(route('files.index'));
    }

    public function testUserCanLoginWithCorrectCredentials()
    {
        $user = User::factory()->create(
            [
                'password' => $password = '123456',
            ]
        );

        $response = $this->post(route('login'), [
            'username' => $user->username,
            'password' => $password,
        ]);

        $this->assertAuthenticatedAs($user);
    }

    public function testUserCannotLoginWithIncorrectPassword()
    {
        $user = User::factory()->create(
            [
                'password' => '123456',
            ]
        );

        $response = $this->post(route('login'), [
            'email' => $user->username,
            'password' => 'invalid-password',
        ]);

        $response->assertSessionHasErrors('username');
        $this->assertGuest();
    }

    public function testUserCannotLoginWithUserNameThatDoesNotExist()
    {
        $response = $this->post(route('login'), [
            'username' => 'fadi',
            'password' => 'invalid-password',
        ]);

        $response->assertSessionHasErrors('username');
        $this->assertGuest();
    }

    public function testUserCanLogout()
    {
        $this->be(User::factory()->create());

        $response = $this->get(route('logout'));

        $this->assertGuest();
    }

    public function testUserCannotLogoutWhenNotAuthenticated()
    {
        $response = $this->get(route('logout'));

        $this->assertGuest();
    }

    public function testUserCannotMakeMoreThanFiveAttemptsInOneMinute()
    {
        $user = User::factory()->create(
            [
                'password' => '123456',
            ]
        );

        foreach (range(0, 5) as $_) {
            $response = $this->post(route('login'), [
                'username' => $user->username,
                'password' => 'invalid-password',
            ]);
        }

        $response->assertSessionHasErrors('username');
        $this->assertMatchesRegularExpression(
            sprintf('/^%s$/', str_replace('\:seconds', '\d+', preg_quote(__('auth.throttle'), '/'))),
            collect(
                $response
                    ->baseResponse
                    ->getSession()
                    ->get('errors')
                    ->getBag('default')
                    ->get('username')
            )->first()
        );
        $this->assertGuest();
    }
}
