<?php

namespace Tests\Feature;

use App\Models\File;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class FilesTest extends TestCase
{
    use RefreshDatabase;

    public function testUserCanListFilesIfAuthenticated()
    {
        $user = User::factory()->create();

        $response = $this->actingAs($user)->get(route('files.index'));
        $response->assertSuccessful();
        $response->assertViewIs('files.index');
    }

    public function testUserCannotListFilesIfUnAuthenticated()
    {
        $response = $this->get(route('files.index'));
        $response->assertRedirect(route('login'));
    }

    public function testUserCanAddFile()
    {
        $user = User::factory()->create();

        $response = $this->actingAs($user)->post(route('files.store'), [
            'file' => UploadedFile::fake()->image('file.png'),
        ]);

        $response->assertRedirect(route('files.index'));

        $file = File::query()->first();
        $this->assertNotNull($file);
        $this->assertFileExists(storage_path("app/$file->path"));
        @unlink(storage_path("app/$file->path"));
    }

    public function testUserCannotAddFileWithoutUploading()
    {
        $user = User::factory()->create();

        $response = $this->actingAs($user)->post(route('files.store'));
        $response->assertSessionHasErrors('file');
    }

    public function testUserCanEditFile()
    {
        Storage::fake();

        $user = User::factory()->create();
        $file = File::factory()->create(['user_id' => $user->id]);
        Storage::put($file->path, '');

        $name = 'fadi';
        $response = $this->actingAs($user)->put(route('files.update', $file), [
            'name' => $name
        ]);

        $response->assertRedirect(route('files.index'));

        $fileAfterUpdate = File::query()->first();
        $this->assertNotNull($fileAfterUpdate);
        $this->assertTrue($fileAfterUpdate->name === $name);
        $this->assertFalse($fileAfterUpdate->name === $file->name);
    }

    public function testUserCannotEditFileWithoutProvidingName()
    {
        Storage::fake();

        $user = User::factory()->create();
        $file = File::factory()->create(['user_id' => $user->id]);
        Storage::put($file->path, '');

        $response = $this->actingAs($user)->put(route('files.update', $file));

        $response->assertInvalid('name');
    }


    public function testUserCannotEditOtherUsersFiles()
    {
        Storage::fake();

        $user1 = User::factory()->create();

        $user2 = User::factory()->create();
        $file = File::factory()->create(['user_id' => $user2->id]);
        Storage::put($file->path, '');

        $name = 'fadi';
        $response = $this->actingAs($user1)->put(route('files.update', $file), [
            'name' => $name
        ]);

        $response->assertForbidden();

        $fileAfterUpdate = File::query()->first();
        $this->assertNotNull($fileAfterUpdate);
        $this->assertFalse($fileAfterUpdate->name === $name);
        $this->assertTrue($fileAfterUpdate->name === $file->name);
    }

    public function testUserCannotEditFileNotExist()
    {
        $user = User::factory()->create();

        $name = 'fadi';
        $response = $this->actingAs($user)->put(route('files.update', 1), [
            'name' => $name
        ]);

        $response->assertNotFound();

        $fileAfterUpdate = File::query()->first();
        $this->assertNull($fileAfterUpdate);
    }

    public function testUserCanDownloadFile()
    {
        Storage::fake();

        $user = User::factory()->create();
        $file = File::factory()->create(['user_id' => $user->id]);
        Storage::put($file->path, '');

        $response = $this->actingAs($user)->get(route('files.show', $file));
        $response->assertDownload("\"$file->name.zip\"");
    }

    public function testUserCannotDownloadOtherUsersFiles()
    {
        Storage::fake();

        $user1 = User::factory()->create();

        $user2 = User::factory()->create();
        $file = File::factory()->create(['user_id' => $user2->id]);
        Storage::put($file->path, '');

        $response = $this->actingAs($user1)->get(route('files.show', $file));
        $response->assertForbidden();
    }

    public function testUserCannotDownloadFileNotExist()
    {
        Storage::fake();

        $user = User::factory()->create();

        $response = $this->actingAs($user)->get(route('files.show', 1));
        $response->assertNotFound();
    }

    public function testUserCanDeleteFile()
    {
        Storage::fake();

        $user = User::factory()->create();
        $file = File::factory()->create(['user_id' => $user->id]);
        Storage::put($file->path, '');

        $response = $this->actingAs($user)->delete(route('files.destroy', $file));
        $response->assertRedirect(route('files.index'));
        $this->assertNull(File::query()->first());
    }

    public function testUserCannotDeleteOtherUsersFiles()
    {
        Storage::fake();

        $user1 = User::factory()->create();

        $user2 = User::factory()->create();
        $file = File::factory()->create(['user_id' => $user2->id]);
        Storage::put($file->path, '');

        $response = $this->actingAs($user1)->delete(route('files.destroy', $file));
        $response->assertForbidden();
    }

    public function testUserCannotDeleteFileNotExist()
    {
        $user = User::factory()->create();

        $response = $this->actingAs($user)->delete(route('files.destroy', 1));
        $response->assertNotFound();
    }
}
